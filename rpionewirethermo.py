#This file was created by Yombo for use with Yombo Gateway automation
#software.  Details can be found at https://yombo.net
"""
Raspberry PI 1Wire Thermo
=========================

Should work on nearly any Raspberry PI model.

Provides support to read 1wire thermostats on a Raspberry PI.

License
=======

See readme.md for details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2016 by Yombo.
:license: MIT
:organization: `Yombo <https://yombo.net>`_
"""
import os
import time
from glob import glob

# Import twisted libraries
from twisted.internet.defer import inlineCallbacks
from twisted.internet.task import LoopingCall

from yombo.core.log import get_logger
from yombo.core.module import YomboModule

logger = get_logger('modules.raspberrypigpio')

DEFAULT_MOUNT_DIR = '/sys/bus/w1/devices/'
DEVICE_FAMILIES = {
    '10': 'DS18S20 - high precision digital thermometer',
    '22': 'DS1822 - echo digital thermometer',
    '28': 'DS18B20 - high precision, programming resolution digital thermometer',
    '3B': 'DS1825 - Programmable Resolution 1-Wire Digital Thermometer with 4 bit ID',
    '42': 'DS28EA00 - 1-Wire Digital Thermometer with Sequence Detect and PIO',
}

class RPIOneWireThermo(YomboModule):
    """
    One Wire thermostat support on raspberry PI
    """
    def _init_(self, **kwargs):
        self.onewire_devices = yield self._SQLDict.get(self, "onewire_devices")  # tracks history of devices found or not.
        self.yombo_devices = self._ModuleDevices()
        self.monitor_devices_loop = None
        self.devices_monitored = {}  # devices we actually monitor

        devices_found  = {}

        for device_family in DEVICE_FAMILIES:
            for device_folder in glob(os.path.join(DEFAULT_MOUNT_DIR, device_family + '[.-]*')):
                devices_found[os.path.split(device_folder)[1]] = \
                    os.path.split(device_folder)[1]

        if len(devices_found) == 0:
            logger.error("Did not find any one wire sensors. Check if 'dtoverlay=w1-gpio' "
                          "is set in /boot/config.txt. ")
            return

        # see if any 1wire devices were added or removed.
        serials_processed = []
        for serial, device_path in devices_found.items():
            serials_processed.append(serial)
            if serial in self.onewire_devices:
                continue
            else:
                self._Notifications.add({
                    'title': 'New one wire device found.',
                    'message': 'New one wire device found. <p>Type: %s<br>Serial: %s<br>' %
                               (DEVICE_FAMILIES[serial[0:2]], serial),
                    'source': 'One Wire Module',
                    'persist': True,
                    'priority': 'high',
                    'always_show': True,
                    'always_show_allow_clear': True,
                    'id': 'onewire_%s' % serial,
                })

        for serial in self.onewire_devices.keys():
            if serial not in serials_processed:
                self._Notifications.add({
                    'title': 'One wire device removed.',
                    'message': 'One wire device removed. <p>Type: %s<br>Serial: %s<br>' %
                               (DEVICE_FAMILIES[serial[0:2]], serial),
                    'source': 'One Wire Module',
                    'persist': True,
                    'priority': 'high',
                    'always_show': True,
                    'always_show_allow_clear': True,
                    'id': 'onewire_%s' % serial,
                })
                del self.onewire_devices[serial]

        # generate list of one wire devices we care about. Where serial is found and in yombo_devices
        for device_id, device in self.yombo_devices.items():
            device_variables = device.device_variables
            print("one wire device_variables: %s" % device_variables)
            device_serial = device_variables['serial']['values'][0]
            if device_serial in devices_found:
                self.devices_monitored[serial] = {
                    'device': device,
                    'onewire_path': devices_found[serial],
                }

    def _start_(self, **kwargs):
        """
        Start monitoring the one wire devices
        :param kwargs:
        :return:
        """
        # for now, we will check the temps every 60 seconds.
        self.monitor_devices_loop = LoopingCall(self.monitor_devices)
        self.monitor_devices_loop.start(60)

    def monitor_devices(self):
        """
        Polls the various one wire devices that we care about.

        :return:
        """
        for serial, device in self.devices_monitored.items():
            print("I should be checking serial path: %s" % device['onewire_path'])

    def _device_command_(self, **kwargs):
        """
        Received a device command.

        :param kwags: Contains 'device' and 'command'.
        :return: None
        """
        device = kwargs['device']
        device_id = device.device_id
        request_id = kwargs['request_id']

        if self._is_my_device(device) is False:
            logger.warn("This module cannot handle device_type_id: {device_type_id}", device_type_id=device.device_type_id)
            return

        # device.device_command_failed(request_id,
        #            message=_('module.raspberrypigpio_failed', 'Tried to send output value to an input.'))
        # command = kwargs['command']
#        device.device_command_done(request_id, message=_('module.raspberrypigpio_done', 'GPIO set.'))

